import datetime
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

import facebook
from twython import Twython
from iron_cache import *

import urlparse

import requests

import time
import urllib2
import urllib
import json
from gcm import *

import hmac
import random
import uuid
import urllib
import json
import hashlib
import time

from firebase.firebase import FirebaseApplication, FirebaseAuthentication
def _generate_signature(data):
    return hmac.new('b4a23f5e39b5929e0666ac5de94c89d1618a2916', data, hashlib.sha256).hexdigest()


def _generate_user_agent():
    resolutions = ['720x1280', '320x480', '480x800', '1024x768', '1280x720', '768x1024', '480x320']
    versions = ['GT-N7000', 'SM-N9000', 'GT-I9220', 'GT-I9100']
    dpis = ['120', '160', '320', '240']

    ver = random.choice(versions)
    dpi = random.choice(dpis)
    res = random.choice(resolutions)

    return (
        'Instagram 4.{}.{} '
        'Android ({}/{}.{}.{}; {}; {}; samsung; {}; {}; smdkc210; en_US)'
    ).format(
        random.randint(1, 2),
        random.randint(0, 2),
        random.randint(10, 11),
        random.randint(1, 3),
        random.randint(3, 5),
        random.randint(0, 5),
        dpi,
        res,
        ver,
        ver,
    )


class InstagramSession(object):

    def __init__(self):
        self.guid = str(uuid.uuid1())
        self.device_id = 'android-{}'.format(self.guid)
        self.session = requests.Session()
        self.session.headers.update({'User-Agent': _generate_user_agent()})

    def login(self, username, password):

        data = json.dumps({
            "device_id": self.device_id,
            "guid": self.guid,
            "username": username,
            "password": password,
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        })
        print data

        sig = _generate_signature(data)

        payload = 'signed_body={}.{}&ig_sig_key_version=4'.format(
            sig,
            urllib.quote_plus(data)
        )

        r = self.session.post("https://instagram.com/api/v1/accounts/login/", payload)
        r_json = r.json()
        print r_json

        if r_json.get('status') != "ok":
            return False

        return True

    def upload_photo(self, filename):
        data = {
            "device_timestamp": time.time(),
        }
        files = {
            "photo": open(filename, 'rb'),
        }

        r = self.session.post("https://instagram.com/api/v1/media/upload/", data, files=files)
        r_json = r.json()
        print r_json

        return r_json.get('media_id')

    def configure_photo(self, media_id, caption):
        data = json.dumps({
            "device_id": self.device_id,
            "guid": self.guid,
            "media_id": media_id,
            "caption": caption,
            "device_timestamp": time.time(),
            "source_type": "5",
            "filter_type": "0",
            "extra": "{}",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        })
        print data

        sig = _generate_signature(data)

        payload = 'signed_body={}.{}&ig_sig_key_version=4'.format(
            sig,
            urllib.quote_plus(data)
        )

        r = self.session.post("https://instagram.com/api/v1/media/configure/", payload)
        r_json = r.json()
        print r_json

        if r_json.get('status') != "ok":
            return False

        return True


def sendPush(current,change,token):
    try:
        if change > 0:
            change = "+" + str(change)[0:2] + "0"
        else:
            change = str(change)[0:2] + "0"
    
        gcm = GCM("AIzaSyCGHtrwa-3uuJ4CNK1fEPI1D12z-9j77eQ")
        
        data = {'currentRate': str(current)[0:3] ,'change':change}
        gcm.plaintext_request(registration_id=token, data=data)   
    except:
        pass
    


def facebookUplad(price):   
    
    cache = IronCache()
 
    # Set the default cache name
    cache.name = "facebookToken" 
    
    access_token_page=cache.get(key="token").value
    FACEBOOK_APP_ID = '916242071742576'
    FACEBOOK_APP_SECRET = '8399e29691e311bf0091454fe1722b0c'
    FACEBOOK_PROFILE_ID = '1398760463749571'

    link = "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=" + FACEBOOK_APP_ID +"&client_secret=" + FACEBOOK_APP_SECRET + "&fb_exchange_token=" + access_token_page
    s = requests.Session()
    token = s.get(link).content
    token = token.split("&")[0]                 # this strips out the expire info (now set set about 5184000 seconds, or 60 days)
    token = token.strip("access_token=")        # Strips out access token

    cache.put(key="token", value=token)


    oauth_args = dict(client_id     = FACEBOOK_APP_ID,
                      client_secret = FACEBOOK_APP_SECRET,
                      grant_type    = 'client_credentials')

    oauth_response = urllib.urlopen('https://graph.facebook.com/oauth/access_token?' + urllib.urlencode(oauth_args)).read()                                  
   
    facebook_graph = facebook.GraphAPI(token)
    try:
        
        response = facebook_graph.put_photo(image=open('facebook.jpg'), message='El Dolar se cotiza en '+price+' Bs')
        #response = facebook_graph.put_object(parent_object=FACEBOOK_PROFILE_ID, connection_name='feed', message='Hello, world')
        #print response
    except facebook.GraphAPIError as e:
        print(e)

def instagram(price):
    insta = InstagramSession()
    if insta.login("dolarvene", "gerswinl3E"):
        media_id = insta.upload_photo("facebook.jpg")
        print media_id
        if media_id is not None:
            insta.configure_photo(media_id, 'El Dolar se cotiza en '+price+' Bs.')


def twitterUpload(price):
    consumer_key = 'cowDntKPSlA8ZZEGyViMmxANU'
    consumer_secret = '099RxPOrGirRNZzzdDuSo16P9VgFm3eY3pC4uZuIWIyUHSvU4B'
    access_token = '3018136767-MRbXiQ5kj3t2kG1ZQd3z23v3BmINvENhz1Jvmdn'
    access_token_secret = 'tKMJtTBQgW4Cq0htNZRq7gbOfob6GboIb0fv4MLlImeek'

    api = Twython(consumer_key,consumer_secret,access_token,access_token_secret)              

    # Send the tweet with photo
    status = 'El Dolar se cotiza en '+price+' Bs.'
    dolarinfo = status + '  Descarga nuestra extension para Chrome http://goo.gl/GxLoqH'

    api.update_profile(description=dolarinfo)

    photo = open('twitter.jpg','rb')
    ids = api.upload_media(media=photo)
    api.update_status(status=status, media_ids=ids['media_id'])

def createImg(price):

    img = Image.open("facebookTemplate.jpg")
    draw = ImageDraw.Draw(img)
    # font = ImageFont.truetype(<font-file>, <font-size>)
    font = ImageFont.truetype("font/libel.ttf", 50)
    # draw.text((x, y),"Sample Text",(r,g,b))
    draw.text((335, 80),price + " BS",(55,189,102),font=font)
    img.save( 'facebook.jpg')
    facebookUplad(price)
    instagram(price)


    img = Image.open("twitterTemplate.jpg")
    draw = ImageDraw.Draw(img)
    # font = ImageFont.truetype(<font-file>, <font-size>)
    font = ImageFont.truetype("font/libel.ttf", 50)
    # draw.text((x, y),"Sample Text",(r,g,b))
    draw.text((310, 57),price + " BS",(55,189,102),font=font)
    img.save( 'twitter.jpg')
    time.sleep(2)
    twitterUpload(price)

    


SECRET = 't9XpbT3ghXXC1KcaCxN9aXAyRVW5CKBlr6CrOvdb'
DSN = 'https://dolarvzla.firebaseio.com'
EMAIL = 'g3rswin@gmail.com'
authentication = FirebaseAuthentication(SECRET,EMAIL, True, True)
firebase = FirebaseApplication(DSN, authentication)

#Obtener el rate de ayer
result = firebase.get('/historical/lastRate', None)
yesterdayRate =  result['lastRate']

#Obtener el LastRate
dolar2 = firebase.get('/dolar2', None)
lastRate =  dolar2['USD']['paralelo']


opener = urllib.FancyURLopener({})
f = opener.open("http://dolartoday.com/custom/rate.js")
js =  f.read()
js = js.replace("var dolartoday =", "").strip()
js = js.replace("dolartoday", "paralelo")
js = js.replace("DOLARTODAY", "PARALELO")

#borrar las cosas que no queremos
data = json.loads(js)
del data['_labels']
del data['_antibloqueo']
#data['_timestamp']['epoch'] = int(time.time())
data['USD']['lastRate'] = lastRate
data['USD']['change'] = float(format(data['USD']['paralelo'] - yesterdayRate, '.2f'))

if (lastRate != data['USD']['paralelo']):
    lastRat =  data['USD']['paralelo']
    simadi =  data['USD']['sicad2']
    changeRate = data['USD']['change']
    ts = int(time.time())
    dlinfo =  {'lastRate': lastRat,'timestamp':ts, 'dolarve':lastRat,'simadi':simadi }
    info =  {'lastRate': lastRat,'timestamp':ts}

    today = time.strftime('%d%m%y')
    firebase.patch('/dolar2', data)
    firebase.post('/historical/pastRates', info)
    firebase.post('/historical/newPastRates', dlinfo)
    firebase.patch('/historical/currentRate', dlinfo)
    createImg(str(lastRat))
    clients = firebase.get('/push', None)
    for tokens in clients:
        try:
            sendPush(lastRat,changeRate,clients[tokens]['token'])
        except:
            print("error token")
            pass

        


   
